<?php
include '/PHPExcel/Classes/PHPExcel.php';
include '/PHPExcel/Classes/PHPExcel/IOFactory.php';

$excel = new PHPExcel(); 

$worksheet = $excel->getActiveSheet(); 

// Set the column styles as the first thing
// from A1 to D1, for 4 column 
$worksheet->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID); 
$worksheet->getStyle('A1:D1')->getFill()->getStartColor()->setRGB('E0E0E0'); 

// Some nice styles for the header (ordinary cell styles) 

$worksheet->getCell('A1')->setValue($_POST['first_column_name']);
$worksheet->getCell('B1')->setValue($_POST['second_column_name']);
$worksheet->getCell('C1')->setValue($_POST['third_column_name']);
$worksheet->getCell('D1')->setValue($_POST['fourth_column_name']);

// record need to be inserted from second row only
$row = 2;

// Cell data        
$worksheet->getCell("A{$row}")->setValue($_POST['first_column_record']);
$worksheet->getCell("B{$row}")->setValue($_POST['second_column_record']);

$excel->getActiveSheet()->getStyle("C{$row}")->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
$worksheet->getCell("C{$row}")->setValue($_POST['third_column_record']);

$excel->getActiveSheet()->getStyle("D{$row}")->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
$worksheet->getCell("D{$row}")->setValue($_POST['fourth_column_record']);
         
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename=generated_excel'.date('Ymdhis').'.xls');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0        

$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007'); 
$writer->save('php://output');
exit;
