<?php
session_start();
include './PHPExcel/Classes/PHPExcel.php';

extract($_POST);

$_SESSION['message'] = '';

if($column_name == '' || $column_name == NULL) {
	$_SESSION['message'] = 'Enter column name';
	header('Location: parse.php');
	exit;
}

	// parsing the excel file
	function parseExcel($excel_file_name_with_path)
	{
		$objPHPExcel = PHPExcel_IOFactory::load($excel_file_name_with_path);
	    $objWorksheet = $objPHPExcel->getActiveSheet();

            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();

            $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
            $headingsArray = $headingsArray[1];
            $r = -1;
            $namedDataArray = array();
            for ($row = 2; $row <= $highestRow; ++$row) {
                $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
                
                // if ((isset($dataRow[$row]['B'])) && ($dataRow[$row]['B'] > '')) {
                if (true) {
                    ++$r;
                    foreach($headingsArray as $columnKey => $columnHeading) {
                        $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                    }
                }
            }

            $isColumn = false;

            foreach ($namedDataArray[0] as $key => $value) {
            	if($_POST['column_name'] == $key) {
            		$isColumn = true;
            	}
            }
            
            if($isColumn == false) {
            	// echo "column not found";
            	return false;
            }

        return ($namedDataArray);
	}

	// validation of uploaded file
	$fileName		= $_FILES['filename']['name'];
	$fileType  		= $_FILES['filename']['type'];
	$fileSize		= $_FILES['filename']['size'];
	$fileTmpName	= $_FILES['filename']['tmp_name'];
	
	if($fileType !="application/excel" && $fileType != 'application/vnd.ms-excel' && $fileType !="application/x-msdownload" && $fileType !="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ) {
		$_SESSION['message'] = 'Invalid File, upload valid file';
		header('Location: parse.php');
		exit;	
	}

	//check moved file if exists and if yes replace the file
	if(file_exists("uploaded_excel_file/".$fileName)) 
	{
		chmod( "uploaded_excel_file/".$fileName,0755); //Change the file permissions if allowed
		unlink( "uploaded_excel_file/".$fileName); //remove the file
	}
	move_uploaded_file ($fileTmpName ,  "uploaded_excel_file/".$fileName);

	$datafile = "uploaded_excel_file/".$fileName;
	
	$data = parseExcel($datafile);

	if(!is_array($data)) {
		$_SESSION['message'] = 'Specified Column not found in Excel file';
		header('Location: parse.php');
		exit;
		echo "not array";
	}

	$index = 0;

	foreach ($data as $value) {

		// retriving the specified column from excel file
		$columnName[$index] = "parse {$index} -> ".trim($value[$column_name]);
		$index++;
	}

	echo "Total Number of Records " . sizeof($columnName);
	echo "<br/><br/><br/>";

	echo '<pre>';

	foreach ($columnName as $key => $value) {
		echo $value . "<br/>";
	}
