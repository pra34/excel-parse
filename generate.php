<?php 
    session_start(); 
    include 'message.php';
?>
<!DOCTYPE html>
<html>
<head>
<title>Excel file Generating</title>
</head>
<body>
    
    <form action="act_generate.php" method="post" enctype="multipart/form-data">
        <font style="font-weight:bold; font-size:14px;">File</font>
        <hr /><br />
        <p>
            <label>First Column Name</label>
            <input type="text" name="first_column_name" id="first_column_name" class="main"/>
        </p>
        <p>
            <label>First Column Record</label>
            <input type="text" name="first_column_record" id="first_column_record" class="main"/>
        </p>
        <p>
            <label>Second Column Name</label>
            <input type="text" name="second_column_name" id="second_column_name" class="main"/>
        </p>
        <p>
            <label>Second Column Record</label>
            <input type="text" name="second_column_record" id="second_column_record" class="main"/>
        </p>
        <p>
            <label>Thrid Column Name</label>
            <input type="text" name="third_column_name" id="third_column_name" class="main"/>
        </p>
        <p>
            <label>Thrid Column Record</label>
            <input type="number" name="third_column_record" id="third_column_record" class="main"/>
        </p>
        <p>
            <label>Fourth Column Name</label>
            <input type="text" name="fourth_column_name" id="fourth_column_name" class="main"/>
        </p>
        <p>
            <label>Fourth Column Record</label>
            <input type="number" name="fourth_column_record" id="fourth_column_record" class="main"/>
        </p>

        <p>
        	<label>&nbsp;</label>
            <input type="submit" name="submit" value="Upload" class="btn" id="upload"/>
            <input type="reset" name="reset" value="Reset" class="btn" />
        </p>
    </form>

<br/>
<input type="submit" value="index" onclick="window.location='index.php';" /> 

</body>
</html>    